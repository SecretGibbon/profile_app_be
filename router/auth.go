package router

import (
	"SecretGibbon/profileApp/handlers"
	"SecretGibbon/profileApp/models"
	"SecretGibbon/profileApp/utils"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
)

type authToken struct {
	Token         string `json:"token"`
}

func GetAuthToken(req *http.Request) (auth authToken, err error) {
	base64EncryptedAuthToken, _, ok := req.BasicAuth()
	if !ok {
		return auth, models.StatusError{
			Code: http.StatusBadRequest,
			Err:  fmt.Errorf("cannot parse basic auth"),
		}
	}

	encryptedAuthToken, err := base64.StdEncoding.DecodeString(base64EncryptedAuthToken)
	if err != nil {
	    return auth, models.StatusError{
			Code: http.StatusBadRequest,
			Err:  fmt.Errorf("cannot parse basic auth"),
		}
	}

	jsonAuthToken, err := utils.Decrypt(encryptedAuthToken)
	if err != nil {
		return auth, models.StatusError{
			Code: http.StatusBadRequest,
			Err:  err,
		}
	}

	var authToken authToken
	err = json.Unmarshal(jsonAuthToken, &authToken)
	return authToken, err
}

func CryptoAuth(next http.Handler) http.Handler {
	auth := func(w http.ResponseWriter, r *http.Request) {
		auth, err := GetAuthToken(r)
		if err != nil {
			utils.WriteError(w, models.StatusError{
				Code: http.StatusUnauthorized,
				Err:  err,
			})
			return
		}

		r = r.WithContext(context.WithValue(r.Context(), handlers.AuthToken, auth.Token))

		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(auth)
}
