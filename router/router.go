package router

import (
	"SecretGibbon/profileApp/utils"
	"github.com/gorilla/mux"
	"github.com/justinas/alice"

	"SecretGibbon/profileApp/handlers"
)


func CreateRouter(env handlers.Env) *mux.Router {
	utils.SetupRender()
	baseMiddleware := alice.New(RecoveryHandler, CommonHeadersHandler)
	middleware := baseMiddleware.Append(CryptoAuth)
	router := mux.NewRouter().StrictSlash(true)

	router.Handle("/login", baseMiddleware.Then(handlers.ErrorMappingHandler{Handle: env.LoginUser})).Methods("POST")
	router.Handle("/google_login", baseMiddleware.Then(handlers.ErrorMappingHandler{Handle: env.LoginGoogleUser})).Methods("POST")

	router.Handle("/profile", middleware.Then(handlers.ErrorMappingHandler{Handle: env.GetProfile})).Methods("GET")
	router.Handle("/profile", middleware.Then(handlers.ErrorMappingHandler{Handle: env.UpdateProfile})).Methods("PATCH")
	router.Handle("/profile", baseMiddleware.Then(handlers.ErrorMappingHandler{Handle: env.CreateProfile})).Methods("PUT")
	router.Handle("/google_profile", baseMiddleware.Then(handlers.ErrorMappingHandler{Handle: env.CreateGoogleProfile})).Methods("PUT")

	router.Handle("/generate_password_reset", baseMiddleware.Then(handlers.ErrorMappingHandler{Handle: env.GeneratePasswordReset})).Methods("POST")
	router.Handle("/reset_password/{token}", baseMiddleware.Then(handlers.ErrorMappingHandler{Handle: env.ValidatePasswordResetToken})).Methods("GET")
	router.Handle("/reset_password", baseMiddleware.Then(handlers.ErrorMappingHandler{Handle: env.ResetPassword})).Methods("POST")
	//router.Handle("/password_reset", middleWare.Then(handlers.ErrorMappingHandler{Handle: env.GetEventMap})).Methods("GET")

	return router
}
