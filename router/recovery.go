package router

import (
	"log"
	"net/http"

	"github.com/go-errors/errors"

	"SecretGibbon/profileApp/config"
)

func RecoveryHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				wrappedErr := errors.Wrap(err, 1)
				log.Println("[ERROR] Panic caught, returning 500 Internal Server Error")
				if config.PropagateInternalErrors {
					log.Printf("[ERROR] HTTP %d - %s", 500, wrappedErr)
					http.Error(w, wrappedErr.Error(), 500)
				} else {
					w.WriteHeader(http.StatusInternalServerError)
				}
			}
		}()
		next.ServeHTTP(w, r)
	})
}
