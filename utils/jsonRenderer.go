package utils

import (
	"SecretGibbon/profileApp/config"
	"SecretGibbon/profileApp/models"
	"net/http"

	"github.com/unrolled/render"
)

var r *render.Render

func SetupRender() {
	r = render.New()
}

func WriteData(w http.ResponseWriter, status int, data interface{}) error {
	return r.JSON(w, status, data)
}

//Writes error response with given status and error
func WriteError(w http.ResponseWriter, err models.StatusError) {
	if config.PropagateInternalErrors {
		w.Header().Set("Content-Type", "application/json")
	} else {
		err.Err = nil
	}
	r.JSON(w, err.Status(), err.Err)
}
