package utils

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"fmt"
	"io"

	"SecretGibbon/profileApp/config"
)

func Encrypt(toEncrypt []byte) (result []byte, err error) {
	c, err := aes.NewCipher([]byte(config.EncryptionKey))
	if err != nil {
		return
	}
	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return
	}
	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return
	}
	result = gcm.Seal(nonce, nonce, toEncrypt, nil)
	return
}

func Decrypt(toDecrypt []byte) (result []byte, err error) {
	c, err := aes.NewCipher([]byte(config.EncryptionKey))
	if err != nil {
		return
	}
	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return
	}
	nonceSize := gcm.NonceSize()
	if len(toDecrypt) < nonceSize {
		return result, fmt.Errorf("cannot decrypt the message")
	}
	nonce, toDecrypt := toDecrypt[:nonceSize], toDecrypt[nonceSize:]
	result, err = gcm.Open(nil, nonce, toDecrypt, nil)
	return
}
