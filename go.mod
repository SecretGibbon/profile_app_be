module SecretGibbon/profileApp

go 1.14

require (
	github.com/go-errors/errors v1.1.1
	github.com/go-gomail/gomail v0.0.0-20160411212932-81ebce5c23df
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/mock v1.4.4
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/jmoiron/sqlx v1.2.0
	github.com/justinas/alice v1.2.0
	github.com/namsral/flag v1.7.4-pre
	github.com/rs/cors v1.7.0
	github.com/stretchr/testify v1.4.0
	github.com/unrolled/render v1.0.3
	golang.org/x/tools v0.0.0-20190425150028-36563e24a262
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df // indirect
	gopkg.in/oauth2.v3 v3.12.0
)
