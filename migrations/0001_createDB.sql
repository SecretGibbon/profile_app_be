-- +goose Up

CREATE TABLE user_profiles (
    user_profile_id INT PRIMARY KEY auto_increment,
    email VARCHAR(255),
    full_name TEXT,
    address TEXT,
    telephone TEXT,
    password TEXT,
    google_id TEXT NULL,
    UNIQUE INDEX up_unique (email)
);
CREATE TABLE user_api_tokens (
    user_profile_id INT,
    token TEXT,
    created_at TIMESTAMP,
    INDEX uat_index (user_profile_id),
    PRIMARY KEY (user_profile_id),
    FOREIGN KEY (user_profile_id) REFERENCES user_profiles(user_profile_id)
        ON DELETE CASCADE
);
CREATE TABLE password_reset_tokens (
    user_profile_id INT,
    token TEXT,
    created_at TIMESTAMP,
    INDEX prt_index (user_profile_id),
    PRIMARY KEY (user_profile_id),
    FOREIGN KEY (user_profile_id) REFERENCES user_profiles(user_profile_id)
       ON DELETE CASCADE
);
DELETE FROM user_profiles;
INSERT INTO user_profiles (email, first_name, last_name, password, google_id) VALUES ('test@test.test', 'tester', 'testerovic', 'MTIzNDU2Nzg5X3Rlc3RAdGVzdC50ZXN0', NULL);
SELECT * from user_profiles;
SELECT * from user_api_tokens;

-- +goose Down
DROP TABLE password_reset_tokens;
DROP TABLE user_api_tokens;
DROP TABLE user_profiles;
