package handlers

import (
	"SecretGibbon/profileApp/models"
	"SecretGibbon/profileApp/services"
)

type Env struct {
	Service services.Service
}

func InitEnv() Env {
	return Env{
		Service: services.Service{
			ProfileStore:       models.NewProfileStore(),
			UserApiTokenStore:  models.NewTokenStore(),
			PasswordResetStore: models.NewPasswordResetStore(),
		},
	}
}
