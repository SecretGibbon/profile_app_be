package handlers

import (
	"SecretGibbon/profileApp/models"
	"SecretGibbon/profileApp/utils"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
)

type (
	GenerateResetTokenRequest struct {
		Email    string `json:"email"`
		HostName string `json:"host_name"`
	}
	PasswordResetRequest struct {
		Token    string `json:"token"`
		Password string `json:"password"`
	}
	ValidatePasswordResetTokenResponse struct {
		Email string `json:"email"`
	}
)

func (env *Env) GeneratePasswordReset(w http.ResponseWriter, r *http.Request) (err error) {
	generateResetTokenRequestJson, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return
	}

	var request GenerateResetTokenRequest
	err = json.Unmarshal(generateResetTokenRequestJson, &request)
	if err != nil {
		return
	}

	err = env.Service.CreatePasswordResetToken(request.Email, request.HostName)
	if err != nil {
		return
	}

	return utils.WriteData(w, http.StatusOK, nil)
}

func (env *Env) ValidatePasswordResetToken(w http.ResponseWriter, r *http.Request) (err error) {
	token, ok := mux.Vars(r)["token"]
	if !ok {
		return models.StatusError{
			Code: http.StatusBadRequest,
			Err:  fmt.Errorf("missing password reset token"),
		}
	}

	email, err := env.Service.ValidatePasswordResetToken(token)
	if err != nil {
		return
	}

	return utils.WriteData(w, http.StatusOK, ValidatePasswordResetTokenResponse{Email: email})
}

func (env *Env) ResetPassword(w http.ResponseWriter, r *http.Request) (err error) {
	defer r.Body.Close()

	encryptedBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return
	}
	passwordResetRequestJson, err := utils.Decrypt(encryptedBytes)
	if err != nil {
		return
	}

	var passwordResetRequest PasswordResetRequest
	err = json.Unmarshal(passwordResetRequestJson, &passwordResetRequest)
	if err != nil {
		return
	}

	err = env.Service.ResetPassword(passwordResetRequest.Token, passwordResetRequest.Password)
	if err != nil {
		return
	}

	return utils.WriteData(w, http.StatusOK, nil)
}
