package handlers

import (
	"SecretGibbon/profileApp/models"
	"SecretGibbon/profileApp/utils"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func (env *Env) GetProfile(w http.ResponseWriter, r *http.Request) error {
	authToken := r.Context().Value(AuthToken)
	if authToken == nil {
		return models.StatusError{
			Code: http.StatusUnauthorized,
			Err:  fmt.Errorf("you are not allowed to view this content"),
		}
	}

	profile, err := env.Service.ProfileStore.GetProfileByToken(authToken.(string))
	if err != nil {
	    return err
	}

	return utils.WriteData(w, http.StatusOK, profile)
}

func (env *Env) UpdateProfile(w http.ResponseWriter, r *http.Request) (err error) {
	authToken := r.Context().Value(AuthToken)
	if authToken == nil {
		return models.StatusError{
			Code: http.StatusUnauthorized,
			Err:  fmt.Errorf("you are not allowed to view this content"),
		}
	}

	profileJson, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return
	}

	var profile models.Profile
	err = json.Unmarshal(profileJson, &profile)
	if err != nil {
		return
	}

	dbProfile, err := env.Service.ProfileStore.GetProfileByToken(authToken.(string))
	if err != nil {
	    return
	}

	if dbProfile.UserProfileID != profile.UserProfileID {
		return models.StatusError{
			Code: http.StatusBadRequest,
			Err:  fmt.Errorf("you are trying to update different profile than your own, that is forbidden"),
		}
	}

	err = env.Service.ProfileStore.UpdateProfile(profile)
	if err != nil {
	    return
	}

	return utils.WriteData(w, http.StatusOK, nil)
}

func (env *Env) CreateProfile(w http.ResponseWriter, r *http.Request) (err error) {
	defer r.Body.Close()
	encryptedBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return
	}
	profileJson, err := utils.Decrypt(encryptedBytes)
	if err != nil {
		return
	}

	var profile models.Profile
	err = json.Unmarshal(profileJson, &profile)
	if err != nil {
		return
	}

	_, err = env.Service.ProfileStore.GetProfileByEmail(profile.Email)
	if err == nil {
		return models.StatusError{
			Code: http.StatusPreconditionFailed,
			Err:  fmt.Errorf("user with this email already exists"),
		}
	}
	if err != sql.ErrNoRows {
		return
	}

	token, err := env.Service.CreateProfile(profile)
	if err != nil {
	    return
	}

	loginResponse := LoginResponse{Token: token}
	loginResponseBytes, err := json.Marshal(loginResponse)
	if err != nil {
		return
	}
	encryptedResponse, err := utils.Encrypt(loginResponseBytes)
	if err != nil {
		return
	}

	w.WriteHeader(http.StatusCreated)
	_, err = w.Write(encryptedResponse)
	return
}

func (env *Env) CreateGoogleProfile(w http.ResponseWriter, r *http.Request) (err error) {
	defer r.Body.Close()
	encryptedBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return
	}
	googleLoginRequestJson, err := utils.Decrypt(encryptedBytes)
	if err != nil {
		return
	}

	var googleLoginRequest models.GoogleLoginRequest
	err = json.Unmarshal(googleLoginRequestJson, &googleLoginRequest)
	if err != nil {
		return
	}

	token, err := env.Service.CreateGoogleProfile(googleLoginRequest)
	if err != nil {
	    return
	}

	loginResponse := LoginResponse{Token: token}
	loginResponseBytes, err := json.Marshal(loginResponse)
	if err != nil {
		return
	}
	encryptedResponse, err := utils.Encrypt(loginResponseBytes)
	if err != nil {
		return
	}

	w.WriteHeader(http.StatusCreated)
	_, err = w.Write(encryptedResponse)
	return
}