package handlers

import (
	"SecretGibbon/profileApp/models"
	"SecretGibbon/profileApp/utils"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type LoginResponse struct {
	Token string
}

func (env *Env) LoginUser(w http.ResponseWriter, r *http.Request) (err error) {
	defer r.Body.Close()

	encryptedBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return
	}
	loginRequestJson, err := utils.Decrypt(encryptedBytes)
	if err != nil {
		return
	}

	var loginRequest models.LoginRequest
	err = json.Unmarshal(loginRequestJson, &loginRequest)
	if err != nil {
		return
	}

	token, err := env.Service.LoginUser(loginRequest)
	if err != nil {
		return err
	}

	loginResponse := LoginResponse{Token: token}
	loginResponseBytes, err := json.Marshal(loginResponse)
	if err != nil {
		return
	}

	encryptedResponse, err := utils.Encrypt(loginResponseBytes)
	if err != nil {
		return
	}
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(encryptedResponse)
	return err
}

func (env *Env) LoginGoogleUser(w http.ResponseWriter, r *http.Request) (err error) {
	defer r.Body.Close()

	encryptedBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return
	}
	googleLoginJson, err := utils.Decrypt(encryptedBytes)
	if err != nil {
		return
	}

	var googleLoginRequest models.GoogleLoginRequest
	err = json.Unmarshal(googleLoginJson, &googleLoginRequest)
	if err != nil {
		return
	}

	token, err := env.Service.LoginGoogleUser(googleLoginRequest)
	if err != nil {
		return err
	}

	loginResponse := LoginResponse{Token: token}
	loginResponseBytes, err := json.Marshal(loginResponse)
	if err != nil {
		return
	}

	encryptedResponse, err := utils.Encrypt(loginResponseBytes)
	if err != nil {
		return
	}

	w.WriteHeader(http.StatusOK)
	_, err = w.Write(encryptedResponse)
	return err
}
