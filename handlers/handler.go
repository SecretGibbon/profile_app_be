package handlers

import (
	"database/sql"
	"log"
	"net/http"

	"SecretGibbon/profileApp/config"
	"SecretGibbon/profileApp/models"
)

// Error represents a handler error. It provides methods for a HTTP status
// code and embeds the built-in error interface.
type Error interface {
	error
	Status() int
}

// The ErrorMappingHandler struct that takes a configured Env and a function matching
// our useful signature.
type ErrorMappingHandler struct {
	Handle func(w http.ResponseWriter, r *http.Request) error
}

// ServeHTTP allows our ErrorMappingHandler type to satisfy http.ErrorMappingHandler.
func (h ErrorMappingHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := h.Handle(w, r)
	if err != nil {
		if err == sql.ErrNoRows {
			err = models.ErrNotFound
		}
		switch e := err.(type) {
		case Error:
			log.Printf("HTTP %d - %s", e.Status(), e)
			http.Error(w, e.Error(), e.Status())
		default:
			log.Printf("[ERROR] %e", err)
			if config.PropagateInternalErrors {
				http.Error(w, e.Error(), http.StatusInternalServerError)
			} else {
				http.Error(w, http.StatusText(http.StatusInternalServerError),
					http.StatusInternalServerError)
			}
		}
	}
}
