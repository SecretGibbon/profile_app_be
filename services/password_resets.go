package services

import (
	"SecretGibbon/profileApp/models"
	"crypto/rand"
	"database/sql"
	"fmt"
	"net/http"
)

func (s *Service) CreatePasswordResetToken(email, hostName string) (err error) {
	profile, err := s.ProfileStore.GetProfileByEmail(email)
	if err != nil {
		if err != sql.ErrNoRows {
			return
		}
		return models.StatusError{
			Code: http.StatusNotFound,
			Err:  fmt.Errorf("given email does not exist"),
		}
	}

	if profile.GoogleID != nil {
		return models.StatusError{
			Code: http.StatusUnprocessableEntity,
			Err:  fmt.Errorf("cannot reset password for google authenticated users"),
		}
	}

	token, err := generateToken()
	if err != nil {
	    return
	}

	err = s.PasswordResetStore.StorePasswordResetToken(profile.UserProfileID, token)
	if err != nil {
	    return
	}

	err = s.SendPasswordResetEmail(profile.Email, profile.FullName, token, hostName)
	return
}

func (s *Service) ValidatePasswordResetToken(token string) (email string, err error) {
	tokenValidation, err := s.PasswordResetStore.ValidatePasswordResetToken(token)
	if err != nil {
		return
	}

	if tokenValidation.IsExpired {
		return "", models.StatusError{
			Code: http.StatusPreconditionFailed,
			Err:  fmt.Errorf("password reset token expired"),
		}
	}
	return tokenValidation.Email, nil
}

func (s *Service) ResetPassword(token string, password string) (err error) {
	tokenValidation, err := s.PasswordResetStore.ValidatePasswordResetToken(token)
	if err != nil {
	    return
	}

	if tokenValidation.IsExpired {
		return models.StatusError{
			Code: http.StatusPreconditionFailed,
			Err:  fmt.Errorf("password reset token expired"),
		}
	}

	profile, err := s.ProfileStore.GetProfileByEmail(tokenValidation.Email)
	if err != nil {
	    return
	}

	profile.Password = &password

	err = s.ProfileStore.UpdateProfile(profile)
	if err != nil {
	    return
	}

	err = s.PasswordResetStore.DeletePasswordResetToken(token)
	return err
}

func generateToken() (token string, err error) {
	b := make([]byte, 32)
	_, err = rand.Read(b)
	if err != nil {
	    return
	}
	return fmt.Sprintf("%x", b), nil
}