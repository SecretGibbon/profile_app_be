package services

import (
	"SecretGibbon/profileApp/config"
	"bytes"
	"crypto/tls"
	"html/template"

	"github.com/go-gomail/gomail"
)

type (
	recipient struct {
		FullName     string
		EmailAddress string
	}
	sendPasswordResetEmailData struct {
		FullName string
		HostName string
		Token string
	}
)

func prepareEmail(subject string, content string, sender string, recipient recipient) (*gomail.Dialer, *gomail.Message) {
	d := gomail.NewDialer(
		config.EmailHost,
		config.EmailPort,
		config.EmailUser,
		config.EmailPassword)
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	m := gomail.NewMessage()
	m.SetHeader("From", sender)
	m.SetAddressHeader("To", recipient.EmailAddress, recipient.FullName)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", content)
	return d, m
}

func parseTemplate(templateFileName string, data interface{}) (result string, err error) {
	t, err := template.ParseFiles(templateFileName)
	if err != nil {
		return result, err
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		return result, err
	}
	return buf.String(), nil
}

func (s *Service) SendPasswordResetEmail(email, fullName, token, hostName string) error {
	subject := "Profile App Password Reset"
	data := sendPasswordResetEmailData{
		FullName: fullName,
		HostName: hostName,
		Token:    token,
	}
	msg, err := parseTemplate("templates/passwordResetEmail.html", data)
	if err != nil {
		return err
	}
	recipient := recipient{
		FullName:     fullName,
		EmailAddress: email,
	}
	return sendEmail(subject, msg, recipient)
}

func sendEmail(subject string, content string, recipient recipient) error {
	dialer, message := prepareEmail(subject, content, config.EmailSender, recipient)
	if err := dialer.DialAndSend(message); err != nil {
		return err
	}
	return nil
}
