package services_test

import (
	"SecretGibbon/profileApp/models"
	"database/sql"
	"encoding/base64"
	"errors"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"net/http"
	"strings"
	"testing"
	"time"

	. "SecretGibbon/profileApp/test"
)

func TestSaveSelectedUserCohort(t *testing.T) {
	mockCtrl, s := PrepareServiceMocks(t)
	defer mockCtrl.Finish()

	password := "1234"
	inputProfile := models.Profile{
		FullName:  "FullName",
		Address:   "Address",
		Telephone: "Telephone",
		Email:     "Email",
		Password:  &password,
	}
	userProfileID := int64(99)
	var generatedToken string

	//Success
	MockProfileStore.EXPECT().GetProfileByEmail(inputProfile.Email).Return(models.Profile{}, sql.ErrNoRows)
	MockProfileStore.EXPECT().CreateProfile(inputProfile).Return(userProfileID, nil)
	MockUserApiTokenStore.EXPECT().StoreToken(userProfileID, gomock.Any()).DoAndReturn(func(upi int64, token string) error {
		generatedToken = token
		return nil
	})

	token, err := s.CreateProfile(inputProfile)
	require.NoError(t, err)
	assert.NotEmpty(t, token)
	assert.Equal(t, generatedToken, token)
	validateTokenAsEmailAndFRC3339NanoTimestamp(t, err, token, inputProfile)

	//Profile for given email already exists
	MockProfileStore.EXPECT().GetProfileByEmail(inputProfile.Email).Return(models.Profile{}, nil)
	_, err = s.CreateProfile(inputProfile)
	require.Error(t, err)
	assert.IsType(t, models.StatusError{}, err)
	assert.Equal(t, http.StatusPreconditionFailed, err.(models.StatusError).Code)

	//Unexpected errors in stores are propagated
	expectedError := errors.New("Unexpected")
	MockProfileStore.EXPECT().GetProfileByEmail(inputProfile.Email).Return(models.Profile{}, expectedError)
	_, err = s.CreateProfile(inputProfile)
	require.Error(t, err)
	assert.Equal(t, expectedError, err)

	MockProfileStore.EXPECT().GetProfileByEmail(inputProfile.Email).Return(models.Profile{}, sql.ErrNoRows)
	MockProfileStore.EXPECT().CreateProfile(inputProfile).Return(userProfileID, expectedError)
	_, err = s.CreateProfile(inputProfile)
	require.Error(t, err)
	assert.Equal(t, expectedError, err)

	MockProfileStore.EXPECT().GetProfileByEmail(inputProfile.Email).Return(models.Profile{}, sql.ErrNoRows)
	MockProfileStore.EXPECT().CreateProfile(inputProfile).Return(userProfileID, nil)
	MockUserApiTokenStore.EXPECT().StoreToken(userProfileID, gomock.Any()).Return(expectedError)
	_, err = s.CreateProfile(inputProfile)
	require.Error(t, err)
	assert.Equal(t, expectedError, err)
}

func validateTokenAsEmailAndFRC3339NanoTimestamp(t *testing.T, err error, token string, inputProfile models.Profile) {
	decodedToken, err := base64.StdEncoding.DecodeString(token)
	require.NoError(t, err)
	decodedTokenString := string(decodedToken)
	strings.HasPrefix(decodedTokenString, inputProfile.Email)
	timestampString := strings.TrimPrefix(decodedTokenString, inputProfile.Email)
	timestamp, err := time.Parse(time.RFC3339Nano, timestampString)
	require.NoError(t, err)
	assert.True(t, timestamp.Before(time.Now()))
}
