package services

import (
	"SecretGibbon/profileApp/models"
	"database/sql"
	"encoding/base64"
	"fmt"
	"net/http"
	"time"
)

func (s *Service) CreateGoogleProfile(googleLoginRequest models.GoogleLoginRequest) (token string, err error) {
	err = s.validateEmailForCreation(googleLoginRequest.GoogleEmail)
	if err != nil {
		return
	}

	userProfileID, err := s.ProfileStore.CreateGoogleProfile(googleLoginRequest.GoogleID, googleLoginRequest.GoogleEmail)
	if err != nil {
		return
	}

	token = base64.StdEncoding.EncodeToString([]byte(googleLoginRequest.GoogleEmail + time.Now().Format(time.RFC3339Nano)))
	err = s.UserApiTokenStore.StoreToken(userProfileID, token)

	return token, err
}

func (s *Service) validateEmailForCreation(email string) (err error) {
	_, err = s.ProfileStore.GetProfileByEmail(email)
	if err == nil {
		return models.StatusError{
			Code: http.StatusPreconditionFailed,
			Err:  fmt.Errorf("user with this email already exists"),
		}
	}
	if err != sql.ErrNoRows {
		return
	}
	return nil
}

func (s *Service) CreateProfile(profile models.Profile) (token string, err error) {
	err = s.validateEmailForCreation(profile.Email)
	if err != nil {
	    return
	}

	userProfileID, err := s.ProfileStore.CreateProfile(profile)
	if err != nil {
		return
	}

	token = base64.StdEncoding.EncodeToString([]byte(profile.Email + time.Now().Format(time.RFC3339Nano)))
	err = s.UserApiTokenStore.StoreToken(userProfileID, token)

	return token, err
}
