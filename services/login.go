package services

import (
	"SecretGibbon/profileApp/models"
	"encoding/base64"
	"fmt"
	"net/http"
	"time"
)

func (s *Service) LoginUser(loginRequest models.LoginRequest) (token string, err error) {
	profile, err := s.ProfileStore.GetProfileByEmail(loginRequest.Username)
	if err != nil {
		return
	}

	if profile.GoogleID != nil {
		return "", models.StatusError{
			Code: http.StatusConflict,
			Err:  fmt.Errorf("user is registered as password user"),
		}
	}

	if profile.Password == nil {
		return "", models.StatusError{
			Code: http.StatusBadRequest,
			Err:  fmt.Errorf("user is authenticated by google"),
		}
	}

	if loginRequest.Password != *profile.Password {
		return "", models.StatusError{
			Code: http.StatusUnauthorized,
			Err:  fmt.Errorf("wrong password"),
		}
	}

	token = base64.StdEncoding.EncodeToString([]byte(loginRequest.Username + time.Now().Format(time.RFC3339Nano)))
	err = s.UserApiTokenStore.StoreToken(profile.UserProfileID, token)

	return
}

func (s *Service) LoginGoogleUser(googleLoginRequest models.GoogleLoginRequest) (token string, err error) {
	profile, err := s.ProfileStore.GetProfileByEmail(googleLoginRequest.GoogleEmail)
	if err != nil {
		return
	}
	if profile.GoogleID == nil || *profile.GoogleID != googleLoginRequest.GoogleID {
		return "", models.StatusError{
			Code: http.StatusConflict,
			Err:  fmt.Errorf("user is registered as password user"),
		}
	}

	token = base64.StdEncoding.EncodeToString([]byte(googleLoginRequest.GoogleEmail + time.Now().Format(time.RFC3339Nano)))
	err = s.UserApiTokenStore.StoreToken(profile.UserProfileID, token)

	return
}
