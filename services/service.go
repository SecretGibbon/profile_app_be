package services

import "SecretGibbon/profileApp/models"

type (
	Service struct {
		ProfileStore       models.ProfileStore
		UserApiTokenStore  models.UserApiTokenStore
		PasswordResetStore models.PasswordResetStore
	}
)
