FROM golang:1.14.7-alpine

ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

RUN apk update
RUN apk add git

WORKDIR /build

COPY go.mod .
COPY go.sum .
RUN go mod download

COPY . .

RUN go build -o profile_app_be ./cmd/api_server/main.go

WORKDIR /dist/templates

COPY templates .

WORKDIR /dist

RUN cp /build/profile_app_be .

EXPOSE 8080

CMD ["/dist/profile_app_be"]
