package models

import (
	"SecretGibbon/profileApp/config"
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type (
	DB interface {
		// Writer returns writable master DB connection.
		Writer() *DbWriter

		// Reader returns read-only DB connection.
		Reader() *DbReader
	}
	DbReader struct {
		DB *sqlx.DB
	}
	DbWriter struct {
		DB *sqlx.DB
	}

	mysqlDB struct {
		db *sqlx.DB
	}
)

var cachedDB DB

func NewDB() DB {
	if cachedDB != nil {
		return cachedDB
	}

	connectionString := createDBConnectionString(
		config.DBHost,
		config.DBPort,
		config.DBUser,
		config.DBPassword,
		config.DBDatabase)
	conn, err := sqlx.Connect("mysql", connectionString)
	if err != nil {
		log.Fatal("Failed to establish DB connection")
	}

	cachedDB = &mysqlDB{db: conn}
	return cachedDB
}

func createDBConnectionString(host, port, user, password, database string) string {
	return user + ":" + password + "@(" + host + ":" + port + ")/" + database
}

func (m *mysqlDB) Writer() *DbWriter {
	return &DbWriter{m.db}
}

func (m *mysqlDB) Reader() *DbReader {
	return &DbReader{m.db}
}

func (db *DbReader) Get(dest interface{}, query string, args ...interface{}) (err error) {
	return db.DB.Get(dest, query, args...)
}

func (db *DbReader) Select(dest interface{}, query string, args ...interface{}) (err error) {
	return db.DB.Select(dest, query, args...)
}

func (db *DbReader) QueryRow(query string, args ...interface{}) (result *sql.Row) {
	return db.DB.QueryRow(query, args...)
}

func (db *DbWriter) Beginx() (*sqlx.Tx, error) {
	return db.DB.Beginx()
}

func (db *DbWriter) Exec(query string, args ...interface{}) (result sql.Result, err error) {
	return db.DB.Exec(query, args...)
}

func (db *DbWriter) Get(dest interface{}, query string, args ...interface{}) (err error) {
	return db.DB.Get(dest, query, args...)
}
