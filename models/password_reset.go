package models

type (
	PasswordResetTokenValidation struct {
		Token         string `db:"token"`
		Email         string  `db:"email"`
		IsExpired     bool   `db:"is_expired"`
	}

	PasswordResetStore interface {
		StorePasswordResetToken(userProfileID int64, token string) (err error)
		DeletePasswordResetToken(token string) (err error)
		ValidatePasswordResetToken(token string) (validation PasswordResetTokenValidation, err error)
	}

	passwordResetStore struct {
		DB DB
	}
)

func NewPasswordResetStore() PasswordResetStore {
	return &passwordResetStore{NewDB()}
}

func (prs *passwordResetStore) StorePasswordResetToken(userProfileID int64, token string) (err error) {
	_, err = prs.DB.Writer().Exec(`
		INSERT INTO password_reset_tokens (user_profile_id, token, created_at)
		VALUES (?, ?, CURRENT_TIMESTAMP)
		ON DUPLICATE KEY UPDATE token = VALUES(token),
			created_at = VALUES(created_at)
	`, userProfileID, token)
	return
}

func (prs *passwordResetStore) DeletePasswordResetToken(token string) (err error) {
	_, err = prs.DB.Writer().Exec(`
		DELETE FROM password_reset_tokens
		WHERE token = ?
	`, token)
	return
}

func (prs *passwordResetStore) ValidatePasswordResetToken(token string) (validation PasswordResetTokenValidation, err error) {
	err = prs.DB.Reader().Get(&validation, `
		SELECT email, token, (CURRENT_TIMESTAMP > created_at + INTERVAL 15 MINUTE) as is_expired 
		FROM password_reset_tokens
		JOIN user_profiles USING (user_profile_id)
		WHERE token = ?
	`, token)
	return
}
