package models

type (
	LoginRequest struct {
		Username string  `json:"username" db:"username"`
		Password string `json:"password" db:"password"`
	}

	GoogleLoginRequest struct {
		GoogleID    string `json:"google_id"`
		GoogleEmail string `json:"google_email"`
	}

	UserApiTokenStore interface {
		StoreToken(userProfileId int64, token string) (err error)
		ValidateToken(userProfileId int64, token string) (isValid bool, err error)
	}

	userApiTokenStore struct {
		DB DB
	}
)

func NewTokenStore() UserApiTokenStore {
	return &userApiTokenStore{NewDB()}
}

func (l *userApiTokenStore) StoreToken(userProfileId int64, token string) (err error) {
	_, err = l.DB.Writer().Exec(`
		INSERT INTO user_api_tokens (user_profile_id, token, created_at)
		VALUES (?, ?, CURRENT_TIMESTAMP)
		ON DUPLICATE KEY UPDATE token = VALUES(token),
			created_at = VALUES(created_at)
		`, userProfileId, token)
	return
}

func (l *userApiTokenStore) ValidateToken(userProfileId int64, token string) (isValid bool, err error) {
	err = l.DB.Reader().Get(&isValid, `
		SELECT COUNT(*) > 0 FROM user_api_tokens
		WHERE user_profile_id = ? AND token = ?
		`, userProfileId, token)
	return
}
