package models

type (
	Profile struct {
		UserProfileID int64   `json:"user_profile_id" db:"user_profile_id"`
		FullName      string  `json:"full_name" db:"full_name"`
		Address       string  `json:"address" db:"address"`
		Telephone     string  `json:"telephone" db:"telephone"`
		Email         string  `json:"email" db:"email"`
		Password      *string `json:"password" db:"password"`
		GoogleID      *string `json:"-" db:"google_id"`
	}

	ProfileStore interface {
		GetProfileByEmail(email string) (Profile, error)
		UpdateProfile(profile Profile) error
		GetProfileByToken(token string) (Profile, error)
		CreateGoogleProfile(googleID, email string) (int64, error)
		CreateProfile(profile Profile) (int64, error)
	}

	profileStore struct {
		DB DB
	}
)

func NewProfileStore() ProfileStore {
	return &profileStore{NewDB()}
}

func (p *profileStore) GetProfileByEmail(email string) (profile Profile, err error) {
	err = p.DB.Reader().Get(&profile, `
		SELECT user_profile_id, full_name, address, telephone, email, password, google_id
		FROM user_profiles
		WHERE email = ?
		`, email)
	return
}

func (p *profileStore) UpdateProfile(profile Profile) (err error) {
	_, err = p.DB.Writer().Exec(`
		UPDATE user_profiles
		SET full_name = ?, address = ?, telephone = ?, email = ?, password = ? 
		WHERE user_profile_id = ?
		`, profile.FullName, profile.Address, profile.Telephone, profile.Email, profile.Password, profile.UserProfileID)
	return
}

func (p *profileStore) GetProfileByToken(token string) (profile Profile, err error) {
	err = p.DB.Reader().Get(&profile, `
		SELECT user_profile_id, full_name, address, telephone, email, password, google_id
		FROM user_profiles
		JOIN user_api_tokens USING (user_profile_id)
		WHERE token = ?
		`, token)
	return
}

func (p *profileStore) CreateGoogleProfile(googleID, email string) (userProfileID int64, err error) {
	_, err = p.DB.Writer().Exec(`
		INSERT INTO user_profiles (full_name, address, telephone, google_id, email)
		VALUES ('', '', '', ?, ?)
		`, googleID, email)
	if err != nil {
		return
	}
	err = p.DB.Reader().Get(&userProfileID, `SELECT LAST_INSERT_ID()`)
	return
}

func (p *profileStore) CreateProfile(profile Profile) (userProfileID int64, err error) {
	_, err = p.DB.Writer().Exec(`
		INSERT INTO user_profiles (full_name, address, telephone, email, password)
		VALUES (?, ?, ?, ?, ?)
		`, profile.FullName, profile.Address, profile.Telephone, profile.Email, profile.Password)
	if err != nil {
		return
	}
	err = p.DB.Reader().Get(&userProfileID, `SELECT LAST_INSERT_ID()`)
	return
}
