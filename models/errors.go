package models

import (
	"fmt"
	"net/http"
)

var (
	ErrNotFound = StatusError{
		Code: http.StatusNotFound,
		Err:  fmt.Errorf("record not found"),
	}
)

type StatusError struct {
	Code int
	Err  error
}

func (se StatusError) Error() string {
	return se.Err.Error()
}

func (se StatusError) Status() int {
	return se.Code
}
