package test

import (
	"testing"

	"github.com/golang/mock/gomock"

	"SecretGibbon/profileApp/services"
	"SecretGibbon/profileApp/test/mocks"
)

var MockProfileStore *mocks.MockProfileStore
var MockPasswordResetStore *mocks.MockPasswordResetStore
var MockUserApiTokenStore *mocks.MockUserApiTokenStore

func PrepareServiceMocks(t *testing.T) (c *gomock.Controller, s services.Service) {
	c = gomock.NewController(t)

	//Profile Store
	MockProfileStore = mocks.NewMockProfileStore(c)
	s.ProfileStore = MockProfileStore
	//Password Reset Store
	MockPasswordResetStore = mocks.NewMockPasswordResetStore(c)
	s.PasswordResetStore = MockPasswordResetStore
	//User Api Token Store
	MockUserApiTokenStore = mocks.NewMockUserApiTokenStore(c)
	s.UserApiTokenStore = MockUserApiTokenStore

	return
}
