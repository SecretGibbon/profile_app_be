package config

import (
	"encoding/json"
	"errors"
	"log"
	"os"

	"github.com/namsral/flag"
)

var (
	ListenAddr                             string
	DBHost                                 string
	DBPort                                 string
	DBUser                                 string
	DBPassword                             string
	DBDatabase                             string
	PasswordResetExpirationPeriodHours     int
	PasswordResetRemovalPeriodHours        int
	RemoveOldPasswordResetsIntervalMinutes int
	CorsAllowedHeaders                     []string
	corsAllowedHeadersJson                 string
	CorsAllowedMethods                     []string
	corsAllowedMethodsJson                 string
	CorsAllowedOrigins                     []string
	corsAllowedOriginsJson                 string
	CorsAllowCredentials                   bool
	CorsOptionsPassthrough                 bool
	CorsDebug                              bool
	PropagateInternalErrors                bool
	EncryptionKey                          string
	EmailHost                              string
	EmailPort                              int
	EmailUser                              string
	EmailPassword                          string
	EmailSender                            string
)

func LoadEnvParams() {
	fs := flag.NewFlagSetWithEnvPrefix(os.Args[0], "PROFILE_APP", flag.ExitOnError)
	fs.StringVar(&ListenAddr, "listen", "127.0.0.1:8080", "TCP listen address/port (default 127.0.0.1:8080)")
	fs.StringVar(&DBHost, "db_host", "", "Database host")
	fs.StringVar(&DBPort, "db_port", "", "Database port")
	fs.StringVar(&DBUser, "db_user", "", "Database user")
	fs.StringVar(&DBPassword, "db_password", "", "Database password")
	fs.StringVar(&DBDatabase, "db_database", "", "Database database name")
	fs.IntVar(&RemoveOldPasswordResetsIntervalMinutes, "remove_old_password_resets_interval_minutes", 5, "Number of minutes in which old password resets are expired and deleted")
	fs.IntVar(&PasswordResetExpirationPeriodHours, "password_reset_expiration_period_hours", 24, "Number of hours after which old password reset should expire")
	fs.IntVar(&PasswordResetRemovalPeriodHours, "password_reset_removal_period_hours", 48, "Number of hours after which old password reset should be deleted after expiration")
	fs.StringVar(&corsAllowedHeadersJson, "cors_allowed_headers", `["X-New-Auth-Token", "Content-Type", "Authorization"]`, "CORS allowed headers for api handlers in json string array format")
	fs.StringVar(&corsAllowedMethodsJson, "cors_allowed_methods", `["HEAD", "GET", "POST", "PUT", "PATCH", "DELETE"]`, "CORS allowed methods for api handlers in json string array format")
	fs.StringVar(&corsAllowedOriginsJson, "cors_allowed_origins", `["http://127.0.0.1", "http://127.0.0.1:8080", "http://localhost", "http://localhost:8080"]`, "CORS origins for api handlers in json string array format")
	fs.BoolVar(&CorsAllowCredentials, "cors_allow_credentials", true, "CORS allow credentials parameter for api handlers")
	fs.BoolVar(&CorsOptionsPassthrough, "cors_options_passthrough", false, "CORS options passthrough parameter for api handlers")
	fs.BoolVar(&CorsDebug, "cors_debug", false, "CORS debug parameter for api handlers")
	fs.BoolVar(&PropagateInternalErrors, "propagate_internal_errors", false, "True if internal server errors should be propagated to clients. DO NOT USE IN PRODUCTION!")
	fs.StringVar(&EncryptionKey, "encryption_key", "", "Encryption key for safe credentials processing")
	fs.StringVar(&EmailHost, "email_host", "", "Hostname of email provider")
	fs.IntVar(&EmailPort, "email_port", 0, "Port of email provider")
	fs.StringVar(&EmailUser, "email_user", "", "Username to use by email provider")
	fs.StringVar(&EmailPassword, "email_password", "", "Password to user by email provider")
	fs.StringVar(&EmailSender, "email_sender", "", "Sender name for all outgoing emails")
	flag.Parse()

	var err error
	if err = fs.Parse(os.Args[1:]); err != nil {
		log.Fatalf("Error parsing arguments %+v: %s\n", os.Args, err.Error())
	}

	if err = validateCfgParams(); err != nil {
		log.Fatalf("Invalid environment configuration: %s", err.Error())
	}

	err = json.Unmarshal([]byte(corsAllowedHeadersJson), &CorsAllowedHeaders)
	if err != nil {
		log.Fatalf("Failed to parse cors_allowed_headers: %e", err)
	}

	err = json.Unmarshal([]byte(corsAllowedMethodsJson), &CorsAllowedMethods)
	if err != nil {
		log.Fatalf("Failed to parse cors_allowed_methods: %e", err)
	}

	err = json.Unmarshal([]byte(corsAllowedOriginsJson), &CorsAllowedOrigins)
	if err != nil {
		log.Fatalf("Failed to parse cors_allowed_origins: %e", err)
	}

	return
}

func validateCfgParams() error {
	if DBHost == "" {
		return errors.New("empty DB host")
	}
	if DBPort == "" {
		return errors.New("empty DB port")
	}
	if DBUser == "" {
		return errors.New("empty DB user")
	}
	if DBPassword == "" {
		return errors.New("empty DB password")
	}
	if DBDatabase == "" {
		return errors.New("empty DB database")
	}
	if PasswordResetExpirationPeriodHours < 1 {
		return errors.New("PasswordResetExpirationPeriodHours must be more than 0")
	}
	if PasswordResetRemovalPeriodHours < 1 {
		return errors.New("PasswordResetRemovalPeriodHours must be more than 0")
	}
	if RemoveOldPasswordResetsIntervalMinutes < 1 {
		return errors.New("RemoveOldPasswordResetsIntervalMinutes must be more than 0")
	}
	if EmailHost == "" {
		return errors.New("EmailHost must not be empty")
	}
	if EmailPort < 1 || EmailPort > 65535 {
		return errors.New("EmailPort must be whole number between 1 and 65535")
	}
	if EmailUser == "" {
		return errors.New("EmailUser must not be empty")
	}
	if EmailPassword == "" {
		return errors.New("EmailPassword must not be empty")
	}
	if EmailSender == "" {
		return errors.New("EmailSender must not be empty")
	}
	if EncryptionKey == "" {
		return errors.New("empty encryption_key")
	}
	return nil
}
